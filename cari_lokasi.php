<?php
	include "koneksi.php";
	
	$hasil 	   = Array();
	$filter_jk = $_POST['filter_jk'];
	$filter_tb = $_POST['filter_tb'];

	try{
		$where = "WHERE status = 'V' ";
		if ($filter_jk != "" || $filter_tb != "") {
			if ($filter_jk != "") {
				$where .= "AND k.id_jenis_kendaraan IN ($filter_jk) ";
			}
			if ($filter_tb != "") {
				$where .= "AND b.id_jenis_tambal IN ($filter_tb) ";
			}
		}

		$stmt = $con->prepare("SELECT DISTINCT t.*
								 FROM tb_tambal_ban t INNER JOIN tb_kat_kendaraan k
								   ON t.id_tambal_ban = k.id_tambal_ban INNER JOIN tb_kat_tambal b
								   ON t.id_tambal_ban = b.id_tambal_ban
								$where
								ORDER BY id_tambal_ban DESC");
		$stmt->execute();
		
		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			array_push($hasil,
				array('id_tambal_ban' => $s["id_tambal_ban"],
					  'alamat' 		  => $s["alamat"],
					  'nama_pemilik'  => $s["nama_pemilik"],
					  'no_telp'		  => $s["no_telp"],
					  'jamb'		  => $s["jam_buka"],
					  'jamt'		  => $s["jam_tutup"],
					  'lat'			  => $s["latitude"],
					  'lng'			  => $s["longitude"],
					  'tarif'		  => $s["tarif"]
				)
			);
		}
		echo json_encode(array("hasil"=>$hasil));
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}	
?>