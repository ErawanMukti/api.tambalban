<?php
	include "koneksi.php";
		
	$arr_jk = Array();
	$arr_tb = Array();

	try{
		$stmt = $con->prepare("SELECT * FROM tb_jenis_kendaraan");
		$stmt->execute();

		$isi = $stmt->fetchAll();
		$tmp = Array();
		foreach($isi as $s){
			array_push($arr_jk, array(
				'id_jenis'   => $s['id_jenis_kendaraan'],
				'nama_jenis' => $s['jenis_kendaraan'],
				'deskripsi'  => $s['deskripsi'],
				'icon'	     => $s['icon']
			));
		}

		$stmt = $con->prepare("SELECT * FROM tb_jenis_tambal");
		$stmt->execute();

		$isi = $stmt->fetchAll();
		$tmp = Array();
		foreach($isi as $s){
			array_push($arr_tb, array(
				'id_jenis'   => $s['id_jenis_tambal'],
				'nama_jenis' => $s['jenis_tambal'],
				'deskripsi'  => $s['deskripsi']
			));
		}
		echo json_encode(array("arr_jk"=>$arr_jk, "arr_tb"=>$arr_tb));
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}	
?>