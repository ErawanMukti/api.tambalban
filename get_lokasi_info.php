<?php
	include "koneksi.php";
	
	$hasil = Array();
	$arrjk = Array();
	$arrtb = Array();
	$id    = $_POST['id'];
	
	try{
		$stmt = $con->prepare('SELECT * FROM tb_tambal_ban WHERE id_tambal_ban = :id');
		$stmt->execute(array(
			':id' => $id
		));
		
		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			if (is_null($s["id_member"])) {
				$id_member = "";
			} else {
				$id_member = $s["id_member"];
			}
			array_push($hasil,
				array('id_tambal_ban' => $s["id_tambal_ban"],
					  'alamat' 		  => $s["alamat"],
					  'nama_pemilik'  => $s["nama_pemilik"],
					  'no_telp'		  => $s['no_telp'],
					  'jamb'		  => $s["jam_buka"],
					  'jamt'		  => $s["jam_tutup"],
					  'lat'		  	  => $s["latitude"],
					  'lng'		  	  => $s["longitude"],
					  'id_member'	  => $id_member
				)
			);
		}
		
		//get jenis kendaraan
		$stmt = $con->prepare('SELECT b.jenis_kendaraan
					 FROM tb_kat_kendaraan a INNER JOIN tb_jenis_kendaraan b
					   ON a.id_jenis_kendaraan = b.id_jenis_kendaraan
					WHERE a.id_tambal_ban = :id');
		$stmt->execute(array(':id' => $id));
		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			array_push($arrjk, array('nama' => $s["jenis_kendaraan"]));
		}
		
		//get jenis tambal ban
		$stmt = $con->prepare('SELECT b.jenis_tambal
					 FROM tb_kat_tambal a INNER JOIN tb_jenis_tambal b
					   ON a.id_jenis_tambal = b.id_jenis_tambal
					WHERE a.id_tambal_ban = :id');
		$stmt->execute(array(':id' => $id));
		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			array_push($arrtb, array('nama' => $s["jenis_tambal"]));
		}
		
		echo json_encode(array("hasil"=>$hasil, "arr_jk"=>$arrjk, "arr_tb"=>$arrtb));
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}	
?>