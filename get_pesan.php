<?php
	include "koneksi.php";
	
	$hasil 	  = Array();
	$member   = $_POST['id'];

	function get_name($id) {
		include "koneksi.php";
		$stmt = $con->prepare('SELECT * FROM tb_member WHERE id_member = :id');
		$stmt->execute(array(
			':id' => $id
		));

		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			$nama = $s['nama'];
		}
		return $nama;
	}

	try{
		$stmt = $con->prepare('SELECT * FROM tb_pesan WHERE pengirim = :member OR penerima  = :member');
		$stmt->execute(array(
			':member' => $member
		));

		$isi = $stmt->fetchAll();
		$tmp = Array();
		foreach($isi as $s){
			$id_pesan = $s["id_pesan"];

			$tmp['id_pesan'] = $id_pesan;
			$tmp['pengirim'] = $s["pengirim"];
			$tmp['penerima'] = $s["penerima"];
			$tmp['nama_pengirim'] = get_name($s["pengirim"]);
			$tmp['nama_penerima'] = get_name($s["penerima"]);

			$stmt2 = $con->prepare('SELECT * FROM tb_pesan_member WHERE id_pesan = :id ORDER BY tanggal DESC LIMIT 0,1');
			$stmt2->execute(array(
				':id' => $id_pesan
			));

			$isi2 = $stmt2->fetchAll();
			foreach($isi2 as $s2){
				$tmp['pesan'] = $s2["pesan"];
				$tmp['tanggal'] = $s2["tanggal"];
			}
			
			array_push($hasil, $tmp);
		}
		echo json_encode(array("hasil"=>$hasil));
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}	
?>