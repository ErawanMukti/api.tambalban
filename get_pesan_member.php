<?php
	include "koneksi.php";
	
	$hasil 	  = Array();
	$hasilId  = Array();
	$id_pesan = $_POST['id_pesan'];
	$pengirim = $_POST['id_pengirim'];
	$penerima = $_POST['id_penerima'];
	
	try{
		if ($id_pesan == "") {
			$stmt = $con->prepare('SELECT * FROM tb_pesan WHERE (pengirim = :pengirim OR penerima = :pengirim) AND (pengirim = :penerima OR penerima = :penerima)');
			$stmt->execute(array(
				':pengirim' => $pengirim,
				':penerima' => $penerima
			));
			$isi = $stmt->fetchAll();

			if (count($isi) == 0) {
				$stmt = $con->prepare('INSERT INTO tb_pesan (pengirim, penerima)
									   VALUES (:pengirim, :penerima)');
				$stmt->execute(array(
					':pengirim' => $pengirim,
					':penerima' => $penerima
				));

				$stmt = $con->prepare('SELECT * FROM tb_pesan WHERE (pengirim = :pengirim OR penerima = :pengirim) AND (pengirim = :penerima OR penerima = :penerima)');
				$stmt->execute(array(
					':pengirim' => $pengirim,
					':penerima' => $penerima
				));
				$isi = $stmt->fetchAll();
			}

			foreach($isi as $s){
				$id_pesan = $s["id_pesan"];
			}
		}
		array_push($hasilId, array('id_pesan' => $id_pesan));

		$stmt2 = $con->prepare('SELECT * FROM tb_pesan_member WHERE id_pesan = :id ORDER BY tanggal ASC');
		$stmt2->execute(array(
			':id' => $id_pesan
		));

		$isi2 = $stmt2->fetchAll();
		foreach($isi2 as $s2){
			array_push($hasil, array(
				'id_member' => $s2["id_member"],
				'pesan' 	=> $s2["pesan"],
				'tanggal' 	=> $s2["tanggal"]
			));
		}

		echo json_encode(array("hasil"=>$hasil, "id" => $hasilId));
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}	
?>