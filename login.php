<?php
	include "koneksi.php";
	
	$email = $_POST['email'];
	$pass  = $_POST['password'];
	$hasil = Array();
	
	try{
		$stmt = $con->prepare('SELECT * FROM tb_member WHERE email = :email AND password = :pass');
		$stmt->execute(array(
			':email' => $email,
			':pass' => $pass
		));
		$isi = $stmt->fetchAll();
		
		if(count($isi) > 0){
			foreach($isi as $s){
				array_push($hasil,
					array('status'	  => 'Berhasil',	
						  'id_member' => $s["id_member"],
						  'nama' 	  => $s["nama"],
						  'pemilik'	  => $s["pemilik"] == 'true' ? 'pemilik' : 'member'
					)
				);
			}
		}else{
			array_push($hasil, array('status' => 'Gagal'));
		}
		echo json_encode(array("hasil"=>$hasil));
	}catch(\PDOException $e){
		array_push($hasil, array('status' => $e->getMessage()));
		echo json_encode(array("hasil"=>$hasil));
	}catch(Exception $e){
		array_push($hasil, array('status' => $e->getMessage()));
		echo json_encode(array("hasil"=>$hasil));
	}
	
?>