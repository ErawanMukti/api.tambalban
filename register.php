<?php
	include "koneksi.php";
	
	$nama	 = $_POST['nama'];
	$email	 = $_POST['email'];
	$pass	 = $_POST['pass'];
	$telepon = $_POST['telepon'];
	$status  = $_POST['status'];

	try{
		$stmt = $con->prepare('INSERT INTO tb_member(nama, no_telp, email, password, pemilik)
							   VALUES(:nama, :telepon, :email, :pass, :status)');
		$stmt->execute(array(
			':nama' 	=> $nama,
			':email' 	=> $email,
			':pass' 	=> $pass,
			':telepon' 	=> $telepon,
			':status' 	=> $status
		));

		echo "Berhasil";
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>