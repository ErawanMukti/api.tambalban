<?php
	include "koneksi.php";
	
    $id 	= $_POST['id'];
    $nama 	= $_POST['nama'];
    $deskripsi 	= $_POST['deskripsi'];
    $icon 	= $_POST['icon'];

	try{
		if ($id == '') {
			$stmt = $con->prepare('INSERT INTO tb_jenis_kendaraan (jenis_kendaraan, deskripsi, icon)
								   VALUES (:nama, :desk, :icon)');
			$stmt->execute(array(
				':nama' => $nama,
				':desk' => $deskripsi,
				':icon' => $icon
			));
		} else {
			$stmt = $con->prepare('UPDATE tb_jenis_kendaraan 
									  SET jenis_kendaraan = :nama,
										  deskripsi = :desk,
										  icon = :icon,
								    WHERE id_jenis_kendaraan = :id');
			$stmt->execute(array(
				':id' => $id,
				':nama' => $nama,
				':desk' => $deskripsi,
				':icon' => $icon
			));
		}
		
		echo "Berhasil";
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>