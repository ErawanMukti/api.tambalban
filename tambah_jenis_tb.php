<?php
	include "koneksi.php";
	
    $id 	   = $_POST['id'];
    $nama 	   = $_POST['nama'];
    $deskripsi = $_POST['deskripsi'];

	try{
		if ($id == '') {
			$stmt = $con->prepare('INSERT INTO tb_jenis_tambal (jenis_tambal, deskripsi)
								   VALUES (:nama, :desk)');
			$stmt->execute(array(
				':nama' => $nama,
				':desk' => $deskripsi
			));
		} else {
			$stmt = $con->prepare('UPDATE tb_jenis_tambal
									  SET jenis_tambal = :nama,
										  deskripsi = :desk
								    WHERE id_jenis_tambal = :id');
			$stmt->execute(array(
				':id' => $id,
				':nama' => $nama,
				':desk' => $deskripsi
			));
		}
		
		echo "Berhasil";
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>