<?php
	include "koneksi.php";
	
    $nama 	= $_POST['nama'];
    $alamat = $_POST['alamat'];
    $lat 	= $_POST['lat'];
    $lng 	= $_POST['lng'];
    $telp 	= $_POST['telp'];
    $jamb 	= $_POST['jamb'];
    $jamt 	= $_POST['jamt'];
    $tarif 	= $_POST['tarif'];
    $jk 	= $_POST['jk'];
    $tb 	= $_POST['tb'];
    $id 	= $_POST['id'];
    $akses 	= $_POST['akses'];

	try{
		if ($akses == 'pemilik') {
			$stmt = $con->prepare(
						'INSERT INTO tb_tambal_ban (id_member, nama_pemilik, no_telp, alamat, latitude, longitude, jam_buka, jam_tutup, tarif, status)
						 VALUES (:id, :nama, :telp, :alamat, :lat, :lng, :jamb, :jamt, :tarif, :status)'
					 );
			$stmt->execute(array(
				':id' 	  => $id,
				':nama'   => $nama,
				':telp'   => $telp,
				':alamat' => $alamat,
				':lat'	  => $lat,
				':lng' 	  => $lng,
				':jamb'   => $jamb,
				':jamt'   => $jamt,
				':tarif'  => $tarif,
				':status' => 'P'
			));
		} else {
			$stmt = $con->prepare(
						'INSERT INTO tb_tambal_ban (nama_pemilik, no_telp, alamat, latitude, longitude, jam_buka, jam_tutup, tarif, status)
						 VALUES (:nama, :telp, :alamat, :lat, :lng, :jamb, :jamt, :tarif, :status)'
					 );
			$stmt->execute(array(
				':nama'   => $nama,
				':telp'   => $telp,
				':alamat' => $alamat,
				':lat' 	  => $lat,
				':lng' 	  => $lng,
				':jamb'   => $jamb,
				':jamt'   => $jamt,
				':tarif'  => $tarif,
				':status' => 'P'
			));
		}

		$stmt = $con->prepare("SELECT id_tambal_ban FROM tb_tambal_ban ORDER BY id_tambal_ban DESC LIMIT 0,1");
		$stmt->execute();
		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			$id_tambal_ban = $s["id_tambal_ban"];
		}

		$arr_jk = explode(",", $jk);
		foreach ($arr_jk as $str_jk) {
			if ($str_jk != "") {
				$stmt = $con->prepare('INSERT INTO tb_kat_kendaraan (id_jenis_kendaraan, id_tambal_ban) VALUES (:id_jenis, :id_tb)');
				$stmt->execute(array(
					':id_jenis' => $str_jk,
					':id_tb'    => $id_tambal_ban
				));
			}
		}

		$arr_tb = explode(",", $tb);
		foreach ($arr_tb as $str_tb) {
			if ($str_tb != "") {
				$stmt = $con->prepare('INSERT INTO tb_kat_tambal (id_jenis_tambal, id_tambal_ban) VALUES (:id_jenis, :id_tb)');
				$stmt->execute(array(
					':id_jenis' => $str_tb,
					':id_tb'    => $id_tambal_ban
				));
			}
		}
		
		echo "Berhasil";
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>