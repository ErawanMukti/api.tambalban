<?php
	include "koneksi.php";
	
    $id_pesan 	= $_POST['id_pesan'];
    $id_member 	= $_POST['id_member'];
    $pesan 		= $_POST['pesan'];
    $tanggal 	= $_POST['tanggal'];

	try{
		$stmt = $con->prepare(
					'INSERT INTO tb_pesan_member (id_pesan, id_member, pesan, tanggal)
					 VALUES (:id_pesan, :id_member, :pesan, :tanggal)'
				 );
		$stmt->execute(array(
			':id_pesan'  => $id_pesan,
			':id_member' => $id_member,
			':pesan' 	 => $pesan,
			':tanggal' 	 => $tanggal
		));
		
		echo "Berhasil";
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>