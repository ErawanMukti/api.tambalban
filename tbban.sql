-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 23, 2017 at 03:37 PM
-- Server version: 5.5.52-cll
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u5593876_tbban`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_kendaraan`
--

CREATE TABLE IF NOT EXISTS `tb_jenis_kendaraan` (
  `id_jenis_kendaraan` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_kendaraan` varchar(15) NOT NULL,
  `deskripsi` text NOT NULL,
  `icon` int(11) NOT NULL,
  PRIMARY KEY (`id_jenis_kendaraan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_jenis_kendaraan`
--

INSERT INTO `tb_jenis_kendaraan` (`id_jenis_kendaraan`, `jenis_kendaraan`, `deskripsi`, `icon`) VALUES
(1, 'Sepeda Motor', '', 1),
(2, 'Mobil Penumpang', '', 3),
(3, 'Sepeda', '', 2),
(4, 'Mobil Barang', '', 4),
(5, 'Kendaraan Khusu', '', 6),
(6, 'Kendaraan Umum', '', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_tambal`
--

CREATE TABLE IF NOT EXISTS `tb_jenis_tambal` (
  `id_jenis_tambal` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_tambal` varchar(15) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_jenis_tambal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_jenis_tambal`
--

INSERT INTO `tb_jenis_tambal` (`id_jenis_tambal`, `jenis_tambal`, `deskripsi`) VALUES
(1, 'Tip Top', ''),
(2, 'Bakar', ''),
(3, 'Cacing', ''),
(4, 'Nitrogen', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kat_kendaraan`
--

CREATE TABLE IF NOT EXISTS `tb_kat_kendaraan` (
  `id_kat_kendaraan` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_kendaraan` int(11) NOT NULL,
  `id_tambal_ban` int(11) NOT NULL,
  PRIMARY KEY (`id_kat_kendaraan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kat_tambal`
--

CREATE TABLE IF NOT EXISTS `tb_kat_tambal` (
  `id_kat_tambal` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_tambal` int(11) NOT NULL,
  `id_tambal_ban` int(11) NOT NULL,
  PRIMARY KEY (`id_kat_tambal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member`
--

CREATE TABLE IF NOT EXISTS `tb_member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `pemilik` varchar(10) NOT NULL,
  PRIMARY KEY (`id_member`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pesan`
--

CREATE TABLE IF NOT EXISTS `tb_pesan` (
  `id_pesan` int(11) NOT NULL AUTO_INCREMENT,
  `pengirim` int(11) NOT NULL,
  `penerima` int(11) NOT NULL,
  PRIMARY KEY (`id_pesan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pesan_member`
--

CREATE TABLE IF NOT EXISTS `tb_pesan_member` (
  `id_pesan_member` int(11) NOT NULL AUTO_INCREMENT,
  `id_pesan` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pesan_member`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tambal_ban`
--

CREATE TABLE IF NOT EXISTS `tb_tambal_ban` (
  `id_tambal_ban` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `nama_pemilik` varchar(30) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `jam_buka` text NOT NULL,
  `jam_tutup` text NOT NULL,
  `tarif` text NOT NULL,
  `status` varchar(1) NOT NULL COMMENT 'v: valid, p: pending, r: reject',
  PRIMARY KEY (`id_tambal_ban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
