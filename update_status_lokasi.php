<?php
	include "koneksi.php";
	
    $id 	= $_POST['id'];
    $status	= $_POST['status'];

	try{
		if ($status == 'H') {
			$stmt = $con->prepare('DELETE FROM tb_tambal_ban WHERE id_tambal_ban = :id');
			$stmt->execute(array(':id' => $id));
		} else {
			$stmt = $con->prepare('UPDATE tb_tambal_ban SET status = :status WHERE id_tambal_ban = :id');
			$stmt->execute(array(
				':id' => $id,
				':status' => $status
			));
		}
		
		echo "Berhasil";
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>